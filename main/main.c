#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_vfs_fat.h"
#include "driver/sdmmc_host.h"

#include "nvs.h"
#include "nvs_flash.h"

#include "aws_iot_config.h"
#include "aws_iot_log.h"
#include "aws_iot_version.h"
#include "aws_iot_mqtt_client_interface.h"
#include "esp_smartconfig.h"

#include "esp_wpa2.h"
#include "esp_event.h"
#include "esp_netif.h"

#define BTN1_GPIO 12
#define BTN2_GPIO 14
#define BTN3_GPIO 13
#define INTR_FLAG_DEFAULT 0

// Frame: 2byte STT, 8 byte Key, 1byte Status
typedef struct data_led {
	uint8_t Command[2];
	uint8_t AppKey[8];
	volatile uint8_t Status;
}data_led_t;

data_led_t btn1;
data_led_t btn2;
data_led_t btn3;

//data_led_t btn1 ={
//		.Command = {0,1},
//		.AppKey = {0,0,0,0,0,0,0,1},
//		.Status = 0,
//};
//
//data_led_t btn2 ={
//		.Command = {0,1},
//		.AppKey = {0,0,0,0,0,0,1,1},
//		.Status = 0,
//};
//
//data_led_t btn3 ={
//		.Command = {0,1},
//		.AppKey = {0,0,0,0,0,1,1,1},
//		.Status = 0,
//};

char rx_buffer[11];
char rx_AppKey[8];
char rx_Status[1];

int flag , flag1, flag2, flag3 = 0;

void triggered_isr(void* instance)
{
//	if(flag == 0){
		if(gpio_get_level(BTN1_GPIO) == 0){
			btn1.Status = !btn1.Status;
			flag1 = 1;
		}
		else if(gpio_get_level(BTN2_GPIO) == 0){
			btn2.Status = !btn2.Status;
			flag2 = 1;
		}
		else if(gpio_get_level(BTN3_GPIO) == 0){
			btn3.Status = !btn3.Status;
			flag3 = 1;
		}
//	}
//	flag = 1;
}

char *hex2str(uint8_t *data, uint8_t len)
{
    static char str[101];
    const char hex[] = "0123456789ABCDEF";
    if (len > 50)
        len = 50;
    for (uint8_t i = 0; i < len; i++)
    {
        str[i * 2] = hex[data[i] >> 4];
        str[i * 2 + 1] = hex[data[i] & 0x0F];
    }
    str[len * 2] = 0;
    return str;
}

//#define EXAMPLE_WIFI_SSID "HUE JSC"
//#define EXAMPLE_WIFI_PASS "Huepress88"

static const char *TAG = "subpub";

static EventGroupHandle_t wifi_event_group;
static const int CONNECTED_BIT = BIT0;
static const int ESPTOUCH_DONE_BIT = BIT1;
static const char *TAG_Sc = "smartconfig";

static void smartconfig_example_task(void * parm);

#if defined(CONFIG_EXAMPLE_EMBEDDED_CERTS)
	extern const uint8_t aws_root_ca_pem_start[] asm("_binary_aws_root_ca_pem_start");
	extern const uint8_t aws_root_ca_pem_end[] asm("_binary_aws_root_ca_pem_end");
	extern const uint8_t certificate_pem_crt_start[] asm("_binary_certificate_pem_crt_start");
	extern const uint8_t certificate_pem_crt_end[] asm("_binary_certificate_pem_crt_end");
	extern const uint8_t private_pem_key_start[] asm("_binary_private_pem_key_start");
	extern const uint8_t private_pem_key_end[] asm("_binary_private_pem_key_end");
#elif defined(CONFIG_EXAMPLE_FILESYSTEM_CERTS)
	static const char * DEVICE_CERTIFICATE_PATH = CONFIG_EXAMPLE_CERTIFICATE_PATH;
	static const char * DEVICE_PRIVATE_KEY_PATH = CONFIG_EXAMPLE_PRIVATE_KEY_PATH;
	static const char * ROOT_CA_PATH = CONFIG_EXAMPLE_ROOT_CA_PATH;
#else
	#error "Invalid method for loading certs"
#endif

char HostAddress[255] = "avwb8v7231rnf-ats.iot.us-east-1.amazonaws.com"; //AWS_IOT_MQTT_HOST;

uint32_t port = 8883; //AWS_IOT_MQTT_PORT;

void iot_subscribe_callback_handler(AWS_IoT_Client *pClient, char *topicName, uint16_t topicNameLen, IoT_Publish_Message_Params *params, void *pData) {
//    ESP_LOGI(TAG, "Subscribe callback");
//    ESP_LOGI(TAG, "%.*s\t%.*s", topicNameLen, topicName, (int) params->payloadLen, (char *)params->payload);

	// Show Data of Topic is subscribed
//	memcpy(rx_buffer, (uint8_t *)params->payload + 0, 11);
//	memcpy(rx_AppKey, rx_buffer+2, 8);
//	memcpy(rx_Status, rx_buffer+10, 1);
//	ESP_LOGI(TAG, "Subcribe TOPIC: %.*s with AppKey: %s, Status: %s", topicNameLen, topicName, hex2str((uint8_t*)rx_AppKey,8), hex2str((uint8_t*)rx_Status,1));

    strncpy(rx_buffer, (char *)params->payload + 0, 11);
    strncpy(rx_AppKey, rx_buffer + 2, 8);
    strncpy(rx_Status, rx_buffer + 10, 1);

    //ESP_LOGI(TAG, "Data %s", rx_buffer);
    ESP_LOGI(TAG, "Subcribe TOPIC: %.*s with AppKey: %s, Status: %s", topicNameLen, topicName, rx_AppKey, rx_Status);
}

void disconnectCallbackHandler(AWS_IoT_Client *pClient, void *data) {
    ESP_LOGW(TAG, "MQTT Disconnect");
    IoT_Error_t rc = FAILURE;

    if(NULL == pClient) {
        return;
    }

    if(aws_iot_is_autoreconnect_enabled(pClient)) {
        ESP_LOGI(TAG, "Auto Reconnect is enabled, Reconnecting attempt will start now");
    } else {
        ESP_LOGW(TAG, "Auto Reconnect not enabled. Starting manual reconnect...");
        rc = aws_iot_mqtt_attempt_reconnect(pClient);
        if(NETWORK_RECONNECTED == rc) {
            ESP_LOGW(TAG, "Manual Reconnect Successful");
        } else {
            ESP_LOGW(TAG, "Manual Reconnect Failed - %d", rc);
        }
    }
}

void aws_iot_task(void *param) {
    IoT_Error_t rc = FAILURE;

    AWS_IoT_Client client;
    IoT_Client_Init_Params mqttInitParams = iotClientInitParamsDefault;
    IoT_Client_Connect_Params connectParams = iotClientConnectParamsDefault;

    ESP_LOGI(TAG, "AWS IoT SDK Version %d.%d.%d-%s", VERSION_MAJOR, VERSION_MINOR, VERSION_PATCH, VERSION_TAG);

    mqttInitParams.enableAutoReconnect = false;
    mqttInitParams.pHostURL = HostAddress;
    mqttInitParams.port = port;

#if defined(CONFIG_EXAMPLE_EMBEDDED_CERTS)
    mqttInitParams.pRootCALocation = (const char *)aws_root_ca_pem_start;
    mqttInitParams.pDeviceCertLocation = (const char *)certificate_pem_crt_start;
    mqttInitParams.pDevicePrivateKeyLocation = (const char *)private_pem_key_start;

#elif defined(CONFIG_EXAMPLE_FILESYSTEM_CERTS)
    mqttInitParams.pRootCALocation = ROOT_CA_PATH;
    mqttInitParams.pDeviceCertLocation = DEVICE_CERTIFICATE_PATH;
    mqttInitParams.pDevicePrivateKeyLocation = DEVICE_PRIVATE_KEY_PATH;
#endif

    mqttInitParams.mqttCommandTimeout_ms = 20000;
    mqttInitParams.tlsHandshakeTimeout_ms = 5000;
    mqttInitParams.isSSLHostnameVerify = true;
    mqttInitParams.disconnectHandler = disconnectCallbackHandler;
    mqttInitParams.disconnectHandlerData = NULL;

#ifdef CONFIG_EXAMPLE_SDCARD_CERTS
    ESP_LOGI(TAG, "Mounting SD card...");
    sdmmc_host_t host = SDMMC_HOST_DEFAULT();
    sdmmc_slot_config_t slot_config = SDMMC_SLOT_CONFIG_DEFAULT();
    esp_vfs_fat_sdmmc_mount_config_t mount_config = {
        .format_if_mount_failed = false,
        .max_files = 3,
    };
    sdmmc_card_t* card;
    esp_err_t ret = esp_vfs_fat_sdmmc_mount("/sdcard", &host, &slot_config, &mount_config, &card);
    if (ret != ESP_OK) {
        ESP_LOGE(TAG, "Failed to mount SD card VFAT filesystem. Error: %s", esp_err_to_name(ret));
        abort();
    }
#endif

    rc = aws_iot_mqtt_init(&client, &mqttInitParams);
    if(SUCCESS != rc) {
        ESP_LOGE(TAG, "aws_iot_mqtt_init returned error : %d ", rc);
        abort();
    }

    /* Wait for WiFI to show as connected */
    xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT, false, true, portMAX_DELAY);

    connectParams.keepAliveIntervalInSec = 10;
    connectParams.isCleanSession = true;
    connectParams.MQTTVersion = MQTT_3_1_1;
    /* Client ID is set in the menuconfig of the example */
    connectParams.pClientID = CONFIG_AWS_EXAMPLE_CLIENT_ID;
    connectParams.clientIDLen = (uint16_t) strlen(CONFIG_AWS_EXAMPLE_CLIENT_ID);
    connectParams.isWillMsgPresent = false;

    ESP_LOGI(TAG, "Connecting to AWS...");
    do {
        rc = aws_iot_mqtt_connect(&client, &connectParams);
        if(SUCCESS != rc) {
            ESP_LOGE(TAG, "Error(%d) connecting to %s:%d", rc, mqttInitParams.pHostURL, mqttInitParams.port);
            vTaskDelay(1000 / portTICK_RATE_MS);
        }
    } while(SUCCESS != rc);
    rc = aws_iot_mqtt_autoreconnect_set_status(&client, true);
    if(SUCCESS != rc) {
        ESP_LOGE(TAG, "Unable to set Auto Reconnect to true - %d", rc);
        abort();
    }

    //Subcribe Topic demo
    const char *TOPIC1 = "Room1/BTN1";
    const char *TOPIC2 = "Room1/BTN2";
    const char *TOPIC3 = "Room1/BTN3";

    ESP_LOGI(TAG, "Subscribing...");
    rc = aws_iot_mqtt_subscribe(&client, TOPIC1, strlen(TOPIC1), QOS1, iot_subscribe_callback_handler, NULL);
    rc = aws_iot_mqtt_subscribe(&client, TOPIC2, strlen(TOPIC2), QOS1, iot_subscribe_callback_handler, NULL);
    rc = aws_iot_mqtt_subscribe(&client, TOPIC3, strlen(TOPIC3), QOS1, iot_subscribe_callback_handler, NULL);
    if(SUCCESS != rc) {
        ESP_LOGE(TAG, "Error subscribing : %d ", rc);
        abort();
    }

    //IoT_Publish_Message_Params paramsQOS0;
    IoT_Publish_Message_Params paramsQOS1;

    char cPayload[100];
//    paramsQOS0.qos = QOS0;
//    paramsQOS0.payload = (void *) cPayload;
//    paramsQOS0.isRetained = 0;

    paramsQOS1.qos = QOS1;
    paramsQOS1.payload = (void *) cPayload;
    paramsQOS1.isRetained = 0;

    while(1) {
        //Max time the yield function will wait for read messages
        rc = aws_iot_mqtt_yield(&client, 400);
        if(NETWORK_ATTEMPTING_RECONNECT == rc) {
            // If the client is attempting to reconnect we will skip the rest of the loop.
            continue;
        }
        //if(flag != 0){
			//ESP_LOGI(TAG, "Stack remaining for task '%s' is %d bytes", pcTaskGetTaskName(NULL), uxTaskGetStackHighWaterMark(NULL));
//			memcpy(cPayload, btn1.Command, 2);
//			memcpy(cPayload + 2, btn1.AppKey, 8);
//			memcpy(cPayload + 10, &btn1.Status, 1);
//			paramsQOS0.payloadLen = strlen(cPayload);
//			aws_iot_mqtt_publish(&client, TOPIC1, strlen(TOPIC1), &paramsQOS0);

        	if(flag1 != 0){
			sprintf(cPayload, "0100000001%d", btn1.Status); //2byte Command, 8byte AppKey, 1byte Status
			paramsQOS1.payloadLen = strlen(cPayload);
			aws_iot_mqtt_publish(&client, TOPIC1, strlen(TOPIC1), &paramsQOS1);
			ESP_LOGW(TAG, "Send TOPIC1 :%s",cPayload);
        	}
        	if(flag2 != 0){
			sprintf(cPayload, "0100000011%d", btn2.Status);
			paramsQOS1.payloadLen = strlen(cPayload);
			aws_iot_mqtt_publish(&client, TOPIC2, strlen(TOPIC2), &paramsQOS1);
			ESP_LOGW(TAG, "Send TOPIC2 :%s",cPayload);
        	}
        	if(flag3 != 0){
			//sprintf(cPayload, "%s%s%d",btn3.Command, btn3.AppKey, btn3.Status);
			sprintf(cPayload, "0100000111%d", btn3.Status);
			paramsQOS1.payloadLen = strlen(cPayload);
			aws_iot_mqtt_publish(&client, TOPIC3, strlen(TOPIC3), &paramsQOS1);
			ESP_LOGW(TAG, "Send TOPIC3 :%s",cPayload);
        	}
			flag1 = 0; flag2 = 0; flag3 = 0;
			//flag = 0;
       // }

        if (rc == MQTT_REQUEST_TIMEOUT_ERROR) {
            ESP_LOGW(TAG, "QOS1 publish ack not received.");
            rc = SUCCESS;
        }
    }
    ESP_LOGE(TAG, "An error occurred in the main loop.");
    abort();
}

static void smartconfig_example_task(void * parm)
{
    EventBits_t uxBits;
    ESP_ERROR_CHECK(esp_smartconfig_set_type(SC_TYPE_ESPTOUCH));
    smartconfig_start_config_t cfg = SMARTCONFIG_START_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_smartconfig_start(&cfg));
    while (1) {
        uxBits = xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT | ESPTOUCH_DONE_BIT, true, false, portMAX_DELAY);
        if(uxBits & CONNECTED_BIT) {
            ESP_LOGI(TAG_Sc, "WiFi Connected to ap");
        }
        if(uxBits & ESPTOUCH_DONE_BIT) {
            ESP_LOGI(TAG_Sc, "smartconfig over");
            esp_smartconfig_stop();
            xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
            vTaskDelete(NULL);
        }
    }
}

static void event_handler_smart_cf(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        xTaskCreate(smartconfig_example_task, "smartconfig_example_task", 4096, NULL, 3, NULL);
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        esp_wifi_connect();
        xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
    } else if (event_base == SC_EVENT && event_id == SC_EVENT_SCAN_DONE) {
        ESP_LOGI(TAG_Sc, "Scan done");
    } else if (event_base == SC_EVENT && event_id == SC_EVENT_FOUND_CHANNEL) {
        ESP_LOGI(TAG_Sc, "Found channel");
    } else if (event_base == SC_EVENT && event_id == SC_EVENT_GOT_SSID_PSWD) {
        ESP_LOGI(TAG_Sc, "Got SSID and password");

        smartconfig_event_got_ssid_pswd_t *evt = (smartconfig_event_got_ssid_pswd_t *)event_data;
        wifi_config_t wifi_config;
        uint8_t ssid[33] = { 0 };
        uint8_t password[65] = { 0 };

        bzero(&wifi_config, sizeof(wifi_config_t));
        memcpy(wifi_config.sta.ssid, evt->ssid, sizeof(wifi_config.sta.ssid));
        memcpy(wifi_config.sta.password, evt->password, sizeof(wifi_config.sta.password));
        wifi_config.sta.bssid_set = evt->bssid_set;
        if (wifi_config.sta.bssid_set == true) {
            memcpy(wifi_config.sta.bssid, evt->bssid, sizeof(wifi_config.sta.bssid));
        }

        memcpy(ssid, evt->ssid, sizeof(evt->ssid));
        memcpy(password, evt->password, sizeof(evt->password));
        ESP_LOGI(TAG_Sc, "SSID:%s", ssid);
        ESP_LOGI(TAG_Sc, "PASSWORD:%s", password);
 
        ESP_ERROR_CHECK( esp_wifi_disconnect() );
        ESP_ERROR_CHECK( esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
        esp_wifi_connect();
        ESP_LOGI(TAG_Sc, "Wifi Connect Done");

        // xEventGroupSetBits(wifi_event_group, ESPTOUCH_DONE_BIT);
        
    } else if (event_base == SC_EVENT && event_id == SC_EVENT_SEND_ACK_DONE) {
        xEventGroupSetBits(wifi_event_group, ESPTOUCH_DONE_BIT); 
    }
}

static void initialise_wifi(void)
{
    ESP_ERROR_CHECK(esp_netif_init());
    wifi_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_t *sta_netif = esp_netif_create_default_wifi_sta();
    assert(sta_netif);

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );

    ESP_ERROR_CHECK( esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler_smart_cf, NULL) );
    ESP_ERROR_CHECK( esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler_smart_cf, NULL) );
    ESP_ERROR_CHECK( esp_event_handler_register(SC_EVENT, ESP_EVENT_ANY_ID, &event_handler_smart_cf, NULL) );

    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK( esp_wifi_start() );
    ESP_LOGI(TAG_Sc, "Init Done");
}

void app_main()
{
    // Initialize NVS.
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    ESP_ERROR_CHECK( err );

    //BTN1
    gpio_pad_select_gpio(BTN1_GPIO);
	gpio_set_direction(BTN1_GPIO, GPIO_MODE_INPUT);
	gpio_set_pull_mode(BTN1_GPIO, GPIO_PULLUP_ONLY);
	gpio_set_intr_type(BTN1_GPIO, GPIO_INTR_NEGEDGE); //GPIO_INTR_ANYEDGE (EDG Down and Up) //GPIO_INTR_NEGEDGE (EDG Down) //GPIO_INTR_POSEDGE (EDG Up) //GPIO_INTR_HIGH_LEVE //GPIO_INTR_LOW_LEVEL
	gpio_install_isr_service(INTR_FLAG_DEFAULT);
	gpio_isr_handler_add(BTN1_GPIO, triggered_isr, NULL);
	gpio_intr_enable(BTN1_GPIO);

    //BTN2
    gpio_pad_select_gpio(BTN2_GPIO);
	gpio_set_direction(BTN2_GPIO, GPIO_MODE_INPUT);
	gpio_set_pull_mode(BTN2_GPIO, GPIO_PULLUP_ONLY);
	gpio_set_intr_type(BTN2_GPIO, GPIO_INTR_NEGEDGE); 
	gpio_install_isr_service(INTR_FLAG_DEFAULT);
	gpio_isr_handler_add(BTN2_GPIO, triggered_isr, NULL);
	gpio_intr_enable(BTN2_GPIO);

    //BTN3
    gpio_pad_select_gpio(BTN3_GPIO);
	gpio_set_direction(BTN3_GPIO, GPIO_MODE_INPUT);
	gpio_set_pull_mode(BTN3_GPIO, GPIO_PULLUP_ONLY);
	gpio_set_intr_type(BTN3_GPIO, GPIO_INTR_NEGEDGE);
	gpio_install_isr_service(INTR_FLAG_DEFAULT);
	gpio_isr_handler_add(BTN3_GPIO, triggered_isr, NULL);
	gpio_intr_enable(BTN3_GPIO);

    initialise_wifi();
    xTaskCreatePinnedToCore(&aws_iot_task, "aws_iot_task", 9216, NULL, 5, NULL, 1);
}
